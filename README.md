Wyqern: a [DRAKON](drakon-editor.com) of a different kind.

We know we've reached version 1.0 when we have reached modelling feature parity 
with both the web and desktop editions of the DRAKON editor.